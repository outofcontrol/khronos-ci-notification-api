<?php

/**
 * Hacking script to mimic a quick API for sending a test email.
 *
 */

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Dotenv\Dotenv;
use SendGrid\Mail\Mail;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Setup
 */
$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/../.env');

$log = new Logger('name');
$log->pushHandler(new StreamHandler(__DIR__ . '/../logs/repository_notifs.log', Logger::DEBUG));

$errors = [];

/** 
 * Verify Source IP is GitLab only
 */
if (!validIP()) {
    $errors = log_message($log, $errors, 'error', 'Access not alloweds');
    echo json_encode($errors, JSON_FORCE_OBJECT);
    exit;
}

/**
 * Got a token?
 */
if (!isset($_GET['token']) || $_GET['token'] != getenv('API_TOKEN')) {
    $errors = log_message($log, $errors, 'error', 'Access not allowed');
    echo json_encode($errors, JSON_FORCE_OBJECT);
    exit;
}

/**
 * Got a message?
 */
if (!isset($_GET['message'])) {
    $msg = "No message was set";
    $errors = log_message($log, $errors, 'warning', 'No message was set');
} else {
    $msg = filter_var($_GET['message'], FILTER_VALIDATE_REGEXP, ["options" => ["regexp" => '/^[a-zA-Z\s]+$/']]);
    $msg = ($msg == '') ? "Invalid message." : $msg;
    if ($msg == "Invalid message.") {
        $errors = log_message($log, $errors, 'warning', 'Invalid message');
    }
}

$email = new \SendGrid\Mail\Mail();
$email->setFrom(getenv('FROM_EMAIL'), getenv('FROM_NAME'));
$email->setSubject(getenv('EMAIL_SUBJECT'));
$email->addTo(getenv('TO_EMAIL'), getenv('TO_NAME'));
$email->addContent("text/plain", $msg);
$email->addContent(
    "text/html",
    "<p>$msg</p>"
);

$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

try {
    $response = $sendgrid->send($email);
    $statusCode = $response->statusCode();
    $message = $response->body();
    $errors = log_message($log, $errors, 'info', "Message sent");
} catch (Exception $e) {
    $errors = log_message($log, $errors, 'error', "Message not sent");
}

echo json_encode($errors, JSON_FORCE_OBJECT);

function log_message($log, $errors, $level, $message)
{
    $log->$level($message);
    $errors[$level][] = $message;
    return $errors;
}

function validIP()
{
    ClientIP::config([]);
    $remote = ClientIP::get();
    return ($remote == getenv('RESTRICT_ACCESS'));
}
